from rest_framework.serializers import (
    ModelSerializer,
    ReadOnlyField
)
from map.models import School


class SchoolSerializer(ModelSerializer):
    class Meta:
        model = School
        fields = ('id', 'name', 'description', 'latitude', 'longitude', 'location')
