from map.models import School


def get_school(school_id):
    return School.objects.get(id=school_id)


def get_schools():
    return School.objects.all()

