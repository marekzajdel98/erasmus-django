from django.views.decorators.http import require_GET
from . import user_api
from django.http import JsonResponse, HttpResponse
from .serializers import SchoolSerializer


@require_GET
def get_school(request, school_id):
    school = user_api.get_school(school_id)
    return JsonResponse(SchoolSerializer(school).data)


@require_GET
def get_schools(request):
    schools = user_api.get_schools()
    return JsonResponse(SchoolSerializer(schools, many=True).data, safe=False)