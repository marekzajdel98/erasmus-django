from django.contrib import admin
from django.urls import path, include
from .views import get_school, get_schools


urlpatterns = [
    path('/schools/<int:school_id>', get_school, name='school-get'),
    path('/schools/', get_schools, name='schools-get'),
]
