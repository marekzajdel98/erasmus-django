# Application for student exchange program

 You need to have Git and [Docker](https://docs.docker.com/install/) installed to run this application.



## Installation

### Clone this repository

```
git clone https://gitlab.com/marekzajdel98/erasmus-django.git
```

### Go to main directory of the project where Dockerfile is located and build a docker image.

```
docker build -t map .
```

### Run the application

```
docker run map
```

### It will start a live server that is available at [localhost:8000](localhost:8000)
