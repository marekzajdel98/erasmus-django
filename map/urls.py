from django.contrib import admin
from django.urls import path
from .views import (
    SchoolListView,
    SchoolDetailView,
    SchoolCreateView,
    SchoolUpdateView,
    SchoolDeleteView,
)
from . import views

urlpatterns = [
    path('', SchoolListView.as_view(), name='map-home'),
    path('schools/<int:pk>', SchoolDetailView.as_view(), name='school-detail'),
    path('schools/create', SchoolCreateView.as_view(), name='school-create'),
    path('schools/<int:pk>/update', SchoolUpdateView.as_view(), name='school-update'),
    path('schools/<int:pk>/delete', SchoolDeleteView.as_view(), name='school-delete'),
    path('map', views.map, name='map-map'),

]
