from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from .models import School


def home(request):
    context = {
        'schools': School.objects.all()
    }
    return render(request, 'map/home.html', context)


class SchoolListView(ListView):
    model = School
    template_name = 'map/home.html'
    context_object_name = 'schools'


class SchoolDetailView(DetailView):
    model = School


class SchoolCreateView(CreateView):
    model = School
    fields = ['name', 'description', 'latitude', 'longitude', 'location']

    def get_initial(self):
        initial_data = super(SchoolCreateView, self).get_initial()
        initial_data['location'] = self.request.GET.get('location')
        initial_data['latitude'] = self.request.GET.get('latitude')
        initial_data['longitude'] = self.request.GET.get('longitude')
        return initial_data
        # now the form will be shown with the link_pk bound to a value


class SchoolUpdateView(UpdateView):
    model = School
    fields = ['name', 'description', 'latitude', 'longitude', 'location']


class SchoolDeleteView(DeleteView):
    model = School
    success_url = '/'


def map(request):

    return render(request, 'map/map.html')

