from django.db import models
from django.urls import reverse


class School(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500)
    created_date = models.DateTimeField(auto_now_add=True)
    latitude = models.FloatField()
    longitude = models.FloatField()
    location = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('school-detail', kwargs={'pk': self.pk})
